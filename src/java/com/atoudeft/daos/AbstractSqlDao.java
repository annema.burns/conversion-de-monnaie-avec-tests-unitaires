/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atoudeft.daos;

import java.sql.Connection;

/**
 *
 * @author moumene
 */
public abstract class AbstractSqlDao<T> implements Dao<T> {
    private Connection connexion;
    public AbstractSqlDao(Connection cnx) {
        this.connexion = cnx;
    }
    public Connection getConnexion() {
        return connexion;
    }
    public void setConnexion(Connection connexion) {
        this.connexion = connexion;
    }
}