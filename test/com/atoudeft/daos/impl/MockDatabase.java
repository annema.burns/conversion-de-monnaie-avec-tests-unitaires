/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atoudeft.daos.impl;

import com.atoudeft.daos.Dao;
import com.atoudeft.entites.TauxMonnaie;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author usager
 */
public class MockDatabase implements Dao<TauxMonnaie> {
    private static final Map<String,TauxMonnaie> monnaies = new HashMap();
    static {  //bloc d'initialisation statique
        monnaies.put("Euro",new TauxMonnaie("Euro",1.5));
        monnaies.put("Livre",new TauxMonnaie("Livre",1.8));
        monnaies.put("$US",new TauxMonnaie("$US",1.25));
        monnaies.put("ITL",new TauxMonnaie("ITL",0.02));
        monnaies.put("Yen",new TauxMonnaie("Yen",0.05));
    }
    @Override
    public List<TauxMonnaie> findAll() {
        return new LinkedList(monnaies.values());
    } 
    @Override
    public TauxMonnaie find(String nomMonnaie) {
        if (monnaies.containsKey(nomMonnaie)) {
            return monnaies.get(nomMonnaie);
        }
        return null;
    }
    @Override
    public TauxMonnaie find(int id) {
        return find(""+id);
    }

    @Override
    public boolean create(TauxMonnaie x) {
        if (monnaies.containsKey(x.getMonnaie())) {
            return false;
        }
        monnaies.put(x.getMonnaie(),x);
        return true;
    }
    @Override
    public boolean delete(TauxMonnaie x) {
        return monnaies.remove(x.getMonnaie()) != null;
    }
    @Override
    public boolean update(TauxMonnaie x) {
        return monnaies.put(x.getMonnaie(),x) != null;
    }   
}
