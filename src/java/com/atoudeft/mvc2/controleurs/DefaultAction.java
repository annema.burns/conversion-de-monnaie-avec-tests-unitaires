/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atoudeft.mvc2.controleurs;

/**
 *
 * @author usager
 */
public class DefaultAction extends AbstractAction {

    @Override
    public String execute() {
        //Cette action par défaut se contente de retourner la vue index correspondant à la page d'accueil.
        return "index";
    }
    
}
