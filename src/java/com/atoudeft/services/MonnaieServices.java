/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atoudeft.services;

import com.atoudeft.daos.Dao;
import com.atoudeft.entites.TauxMonnaie;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author AToudeft
 */
public class MonnaieServices {
    private static Dao<TauxMonnaie> dao;

    public static void setDao(Dao dao) {
        MonnaieServices.dao = dao;
    }
    
    public static List<String> getNomsMonnaies() {
        List<String> monnaies = new LinkedList();
        ListIterator<TauxMonnaie> iListe = dao.findAll().listIterator();
        while (iListe.hasNext()) {
            monnaies.add(iListe.next().getMonnaie());
        }
        return monnaies;            
    }
    public static double doConversion(String monnaie, double montant) throws UnsupportedOperationException{
            double montantEquivalent;
            TauxMonnaie tm = dao.find(monnaie);
            if (tm == null){ //monnaie inexistante
                throw new UnsupportedOperationException("Monnaie de conversion non supportée."); 
            }
            double taux = tm.getValeur();
            montantEquivalent = montant * taux;
            return montantEquivalent;
    } 
}
