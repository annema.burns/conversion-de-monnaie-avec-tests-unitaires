/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atoudeft.mvc2.controleurs;

import com.atoudef.mock.MockHTTPServletRequest;
import com.atoudef.mock.MockHTTPServletResponse;
import com.atoudeft.daos.Dao;
import com.atoudeft.ecouteurs.EcouteurApplication;
import com.atoudeft.services.MonnaieServices;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aburns
 */
public class ConversionActionTest {
    
    ConversionAction action;
    MockHTTPServletRequest mockRequest;
    MockHTTPServletResponse mockResponse;
    
    public ConversionActionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        // initialiser le service afin qu'il utilise la simulation de 
        // la base de données 
        String daoClass = "com.atoudeft.daos.impl.MockDatabase";
        
        try {
            Dao dao = (Dao) Class.forName(daoClass).newInstance();
            MonnaieServices.setDao(dao);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(EcouteurApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Crée des simulations pour HTTPServletRequest et HTTPServletResponse
        mockRequest = new MockHTTPServletRequest();
        mockResponse = new MockHTTPServletResponse();
        
        // intialiser le contrôleur afin qu'il utilise les simulations de 
        // HTTPServletRequest et HTTPServletResponse        
        action = new ConversionAction();
        action.setRequest(mockRequest);
        action.setResponse(mockResponse);
        
        // intialise avec les paramètres à tester
        // action à tester
        mockRequest.setParameter("action", new String[]{"convertir"});

    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class ConversionAction.
     */
    @Test
    public void testExecute() {
        
        // intialise avec les paramètres à tester
        mockRequest.setParameter("montant", new String[]{"3"});
        mockRequest.setParameter("monnaie", new String[]{"Euro"}); 
        
        String vue = action.execute();
        
        assertEquals("index", vue);
        assertEquals((double)mockRequest.getAttribute("RESULTAT"), 4.5, 0.001);
    }
    
    /**
     * Test of execute method, of class ConversionAction.
     */
    @Test
    public void testExecuteMonnaieNonSupportee() {
        
        // intialise avec les paramètres à tester
        mockRequest.setParameter("montant", new String[]{"3"});
        mockRequest.setParameter("monnaie", new String[]{"Franc"}); 
        
        String vue = action.execute();
        
        assertEquals("index", vue);
        assertEquals(mockRequest.getAttribute("MESSAGE_ERREUR"), "Monnaie de conversion non supportée.");
    }
    
    /**
     * Test of execute method, of class ConversionAction.
     */
    @Test
    public void testExecuteMontantNonValide() {
        
        // intialise avec les paramètres à tester
        mockRequest.setParameter("montant", new String[]{"a"});
        mockRequest.setParameter("monnaie", new String[]{"Euro"}); 
        
        String vue = action.execute();
        
        assertEquals("index", vue);
        assertEquals(mockRequest.getAttribute("MESSAGE_ERREUR"), "Montant à convertir non valide.");
    }

    /**
     * Test of execute method, of class ConversionAction.
     */
    @Test
    public void testExecuteMontantNonSpecifie() {
        
        // intialise avec les paramètres à tester
        mockRequest.setParameter("monnaie", new String[]{"Euro"}); 
        
        String vue = action.execute();
        
        assertEquals("index", vue);
        assertEquals(mockRequest.getAttribute("MESSAGE_ERREUR"), "Montant à convertir non spécifié.");
    }

    /**
     * Test of execute method, of class ConversionAction.
     */
    @Test
    public void testExecuteMonnaieNonSpecifiee() {
        
        // intialise avec les paramètres à tester
        mockRequest.setParameter("montant", new String[]{"3"});
         
        String vue = action.execute();
        
        assertEquals("index", vue);
        assertEquals(mockRequest.getAttribute("MESSAGE_ERREUR"), "Monnaie de conversion non spécifiée.");
    }

    /**
     * Test of execute method, of class ConversionAction.
     */
    @Test
    public void testExecuteMonnaieVide() {
        
        // intialise avec les paramètres à tester
        mockRequest.setParameter("monnaie", new String[]{""});
        mockRequest.setParameter("montant", new String[]{"3"});
         
        String vue = action.execute();
        
        assertEquals("index", vue);
        assertEquals(mockRequest.getAttribute("MESSAGE_ERREUR"), "Monnaie de conversion non spécifiée.");
    }
}
