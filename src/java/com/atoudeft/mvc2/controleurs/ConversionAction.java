/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atoudeft.mvc2.controleurs;

import org.apache.commons.validator.routines.DoubleValidator;
import com.atoudeft.services.MonnaieServices;

/**
 *
 * @author AToudeft
 */
public class ConversionAction extends AbstractAction {

    @Override
    public String execute() {
        //Cette action effectue la conversion désirée et retourne la vue index 
        //correspondant à la page d'accueil, qui va afficher le résultat de la conversion.
        
        // récupère la monnaie de conversion
        String monnaieChoisie = request.getParameter("monnaie");
        // vérifie qu'elle est spécifiée
        if(monnaieChoisie != null && !monnaieChoisie.equals("")) {
            
            // récupère le montant à convertir
            String montantSaisi = request.getParameter("montant");
            // vérifie que le montant est bien spécifié
            if(montantSaisi != null) {
                
                // vérifie que le montant est valide
                final DoubleValidator dv = new DoubleValidator();
                if(dv.isValid(montantSaisi)) {
                    
                    // converti le montant et effectue le calcul
                    double montant = dv.validate(montantSaisi);
                    
                    try {
                        double montantEquivalent = MonnaieServices.doConversion(monnaieChoisie, montant);
                    
                        //On injecte dans la requête le résultat de la conversion pour que 
                        //la vue index le retrouve et l'affiche :
                        request.setAttribute("RESULTAT", montantEquivalent);
                    
                    } catch(UnsupportedOperationException e) {
                        request.setAttribute("MESSAGE_ERREUR", e.getMessage());
                    } catch(Exception e) {
                        request.setAttribute("MESSAGE_ERREUR", "Service temporairement non disponible");
                    }
                    
                    
                } else {
                    request.setAttribute("MESSAGE_ERREUR", "Montant à convertir non valide.");
                }
                
            } else {
                request.setAttribute("MESSAGE_ERREUR", "Montant à convertir non spécifié.");
            }
            
        } else {
            request.setAttribute("MESSAGE_ERREUR", "Monnaie de conversion non spécifiée.");
        }

        return "index"; //On retourne la vue index
    }
}
