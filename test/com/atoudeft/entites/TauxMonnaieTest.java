/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atoudeft.entites;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aburns
 */
public class TauxMonnaieTest {
    // valeurs attendues
    final static String NOM_MONNAIE = "Euro";
    final static double TAUX_MONNAIE = 1.5;
        
    TauxMonnaie monnaie;
    
    public TauxMonnaieTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        // création de l'objet
        monnaie = new TauxMonnaie(NOM_MONNAIE, TAUX_MONNAIE);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test du constructeur de la classe TauxMonnaie.
     */
    @Test
    public void testCreationTauxMonnaie() {
        // validation du résultat
        assertEquals(monnaie.getMonnaie(), NOM_MONNAIE);
        assertEquals(monnaie.getValeur(), TAUX_MONNAIE, 0.0000000001);
    }
    
    /**
     * Test du constructeur de la classe TauxMonnaie avec une valeur négative.
     */
    @Test (expected=IllegalArgumentException.class)
    public void testCreationTauxMonnaieValeurNegative() {
        
        monnaie = new TauxMonnaie(NOM_MONNAIE, -TAUX_MONNAIE);
    }

    /**
     * Test of setMonnaie method, of class TauxMonnaie.
     */
    @Test
    public void testSetMonnaie() {
        // Modification du nom
        final String nvNomMonnaie = "US$";
        
        monnaie.setMonnaie(nvNomMonnaie);
        
        // validation du résultat
        assertEquals(monnaie.getMonnaie(), nvNomMonnaie);
    }

    /**
     * Test of setValeur method, of class TauxMonnaie.
     */
    @Test
    public void testSetValeur() {
        // Modification du taux
        double nvTauxMonnaie = 1.2;
        
        monnaie.setValeur(nvTauxMonnaie);
        
        // validation du résultat
        assertEquals(monnaie.getValeur(), nvTauxMonnaie, 0.0000000001);
    }
    
    @Test (expected=IllegalArgumentException.class)
    public void testSetValeurTauxNegatif() { 
        // Modification du taux
        double nvTauxMonnaie = -1.2;
        
        monnaie.setValeur(nvTauxMonnaie);
        
    }
    
}
