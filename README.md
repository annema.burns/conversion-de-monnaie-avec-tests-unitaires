# Conversion de monnaie avec tests unitaires

Cet exemple reprend la solution proposée par A. Toudef pour le  laboratoire du chapitre 9 du cours hypermédia 2, session automne 2019.

Des tests unitaires sont ajoutés à la solution afin d'en démontrer l'utilisation dans le cadre du cours de développement de projets informatiques, session automne 2019.

Configuration du lien avec une BD MySQL

fichier web.xml (effacer(Windows)/remplacer le port au besoin, ici :8889)

<pre>
<code>
&lt;context-param&gt;
    &lt;param-name&gt;urlBd&lt;/param-name&gt;
    &lt;param-value&gt;jdbc:mysql://localhost:8889/conversion?serverTimezone=UTC&lt;/param-value&gt;
&lt;/context-param&gt;
</code>
</pre>