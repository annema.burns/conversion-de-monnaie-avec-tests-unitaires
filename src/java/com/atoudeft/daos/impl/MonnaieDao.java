/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.atoudeft.daos.impl;

import com.atoudeft.daos.AbstractSqlDao;
import com.atoudeft.entites.TauxMonnaie;
import com.atoudeft.singleton.DbConnexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Moumene Toudeft <moumenet at gmail.com>
 */
public class MonnaieDao extends AbstractSqlDao<TauxMonnaie> {

    public MonnaieDao() {
        super(null);
    }
    public MonnaieDao(Connection cnx) {
        super(cnx);
    }
    @Override
    public List<TauxMonnaie> findAll() {
        Connection cnx = DbConnexion.getConnexion();
        List<TauxMonnaie> liste = new LinkedList();
        TauxMonnaie tm;
        try {
            Statement stm = cnx.createStatement();
            ResultSet res = stm.executeQuery("SELECT * FROM monnaie");
            while (res.next()) {
                tm = new TauxMonnaie();
                tm.setMonnaie(res.getString("nom"));
                tm.setValeur(res.getDouble("valeur"));
                liste.add(tm);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MonnaieDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return liste;
    }
    @Override
    public TauxMonnaie find(String nomMonnaie) {
        Connection cnx = DbConnexion.getConnexion();
        TauxMonnaie tm = null;
        try {
            //Exemple de requête paramétrée :
            PreparedStatement stm = cnx.prepareStatement("SELECT * FROM monnaie WHERE nom=?");
            stm.setString(1, nomMonnaie);
            ResultSet res = stm.executeQuery();
            if (res.next()) {
                tm = new TauxMonnaie();
                tm.setMonnaie(res.getString("nom"));
                tm.setValeur(res.getDouble("valeur"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MonnaieDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tm;
    }
    @Override
    public TauxMonnaie find(int id) {
        return find(""+id);
    }

    @Override
    public boolean create(TauxMonnaie x) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(TauxMonnaie x) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(TauxMonnaie x) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
