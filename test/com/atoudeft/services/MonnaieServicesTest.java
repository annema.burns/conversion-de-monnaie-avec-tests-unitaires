/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atoudeft.services;

import com.atoudeft.daos.Dao;
import com.atoudeft.daos.impl.MockDatabase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aburns
 */
public class MonnaieServicesTest {
    Dao dao = new MockDatabase();
    
    public MonnaieServicesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MonnaieServices.setDao(dao);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNomsMonnaies method, of class MonnaieServices.
     */
    @Test
    public void testGetNomsMonnaies() {
        assertEquals(MonnaieServices.getNomsMonnaies().size(), 5);
        assertTrue(MonnaieServices.getNomsMonnaies().contains("Euro"));
        assertTrue(MonnaieServices.getNomsMonnaies().contains("Livre"));
    }

    /**
     * Test of doConversion method, of class MonnaieServices.
     */
    @Test
    public void testDoConversion() {
        String monnaie = "Euro";
        double montant = 3.0;
        double expResult = 4.5;
        double result = MonnaieServices.doConversion(monnaie, montant);
        assertEquals(expResult, result, 0.001);
    }
    
    /**
     * Test of doConversion method, of class MonnaieServices.
     */
    @Test (expected=UnsupportedOperationException.class)
    public void testDoConversionMonnaieInexistante() {
        String monnaie = "Eur";
        double montant = 3.0;
        double expResult = 0;
        double result = MonnaieServices.doConversion(monnaie, montant);
    }
    
}
