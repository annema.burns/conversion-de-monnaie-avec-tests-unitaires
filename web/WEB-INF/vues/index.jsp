<%-- 
    Document   : index
    Created on : 2018-07-09, 16:05:59
    Author     : AToudeft
--%>

<%@page import="java.util.List"%>
<%@page import="com.atoudeft.services.MonnaieServices"%>
<%@page import="java.util.ListIterator"%>
<%@page import="com.atoudeft.entites.TauxMonnaie"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="./static/css/style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%
            ListIterator<String> it = null;
            String uneMonnaie;
            try {
                List<String> liste = MonnaieServices.getNomsMonnaies();
                it = liste.listIterator();
                
            } catch(Exception e) {
                request.setAttribute("MESSAGE_ERREUR", "Service temporairement non disponible");
            }
        %>
        <h1>Convertisseur de monnaies</h1>
        <form>
            <input type="hidden" name="action" value="convertir" />
            Montant : <input type="text" name="montant" />
            <select name="monnaie">
                <option value="">--Choisir--</option>
                <%
                    while (it != null && it.hasNext()){
                        uneMonnaie = it.next();
                %>
                <option value="<%=uneMonnaie%>"><%=uneMonnaie%></option>
                <%
                    }
                %>
            </select>
            <input type="submit" value="OK" />
        </form>
        <%
            if (request.getAttribute("RESULTAT") != null) {
                out.println("Montant équivalent : <span class='resultat'>"+request.getAttribute("RESULTAT")+" $CAN</span>");
            }
            else if (request.getAttribute("MESSAGE_ERREUR") != null) {
                out.println("<span class='erreur'>"+request.getAttribute("MESSAGE_ERREUR")+"</span>");
            }
        %>
    </body>
</html>
