/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.atoudeft.entites;

/**
 * 
 * @author Moumene Toudeft <moumenet at gmail.com>
 */
public class TauxMonnaie {
    private String monnaie;
    private double valeur;

    public TauxMonnaie() {
    }

    public TauxMonnaie(String monnaie, double valeur) throws IllegalArgumentException {
        this.setMonnaie(monnaie);
        this.setValeur(valeur);
    }

    public String getMonnaie() {
        return monnaie;
    }

    public void setMonnaie(String monnaie) {
        this.monnaie = monnaie;
    }

    public double getValeur() {
        return valeur;
    }

    public void setValeur(double valeur) throws IllegalArgumentException {
        if(valeur < 0) {
            throw new IllegalArgumentException("Le taux de conversion de la monnaie ne peut pas être négatif");
        }
        this.valeur = valeur;
    }   
}
