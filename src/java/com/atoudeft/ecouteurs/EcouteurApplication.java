/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atoudeft.ecouteurs;

import com.atoudeft.daos.Dao;
import com.atoudeft.services.MonnaieServices;
import com.atoudeft.singleton.DbConnexion;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author usager
 */
public class EcouteurApplication implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Démarrage de l'application ["
                + sce.getServletContext().getServletContextName()
                + "].");
        DbConnexion.loadDriver(sce.getServletContext().getInitParameter("pilote"));
        DbConnexion.setUrlBD(sce.getServletContext().getInitParameter("urlBd"));
        String user = sce.getServletContext().getInitParameter("dbUser");
        if (user != null && !"".equals(user)) {
            DbConnexion.setUser(user);
            DbConnexion.setPassword(sce.getServletContext().getInitParameter("dbPassword"));
        } else {
            DbConnexion.setUser("");
        }
        String daoClass = sce.getServletContext().getInitParameter("daoClass");
        if (daoClass == null) {
            System.out.println("WARNING : classe du DAO non spécifiée dans web.xml. 'com.atoudeft.daos.impl.MockDatabase' est prise par défaut");
            daoClass = "com.atoudeft.daos.impl.MockDatabase";
        }
        try {
            Dao dao = (Dao) Class.forName(daoClass).newInstance();
            MonnaieServices.setDao(dao);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(EcouteurApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Application arrêtée");
        DbConnexion.close();
    } 
}
