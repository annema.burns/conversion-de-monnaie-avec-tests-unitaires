/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atoudeft.mvc2.controleurs;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aburns
 */
public class DefaultActionTest {
    final static String DEFAULT_ACTION = "index";
    
    DefaultAction action;
    
    public DefaultActionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        action = new DefaultAction();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class DefaultAction.
     */
    @Test
    public void testExecute() {
        String vue = action.execute();
        
        assertEquals(DEFAULT_ACTION, vue);
    }
    
}
