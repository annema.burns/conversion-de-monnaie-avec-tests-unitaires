/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atoudeft.daos;

import java.util.List;

/**
 *
 * @author moumene
 */
public interface Dao<T> {
    public boolean create(T x);
    public boolean delete(T x);
    public boolean update(T x);
    public T find(int id);
    public T find(String id);
    public List<T> findAll();    
}
